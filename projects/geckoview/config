# vim: filetype=yaml sw=2
version: '[% c("abbrev") %]'
filename: 'geckoview-[% c("version") %]-[% c("var/osname") %]-[% c("var/build_id") %].tar.gz'
git_hash: 'tor-browser-[% c("var/geckoview_version") %]-[% c("var/browser_branch") %]-build[% c("var/browser_build") %]'
tag_gpg_id: 1
git_url: https://gitlab.torproject.org/tpo/applications/tor-browser.git
gpg_keyring:
  - pierov.gpg
  - richard.gpg
container:
  use_container: 1

var:
  geckoview_version: 102.10.0esr
  browser_branch: 12.5-1
  browser_build: 3
  copyright_year: '[% exec("git show -s --format=%ci").remove("-.*") %]'
  deps:
    - build-essential
    - unzip
    - zip
    - autoconf2.13
    - yasm
    - python3
    - python3-distutils
    - pkg-config
    - openjdk-11-jdk-headless
  # this should be updated when the list of gradle dependencies is changed
  # see doc/how-to-create-gradle-dependencies-list.txt
  gradle_dependencies_version: 11
  gradle_version: 7.3

steps:
  merge_aars:
    filename: 'geckoview-[% c("version") %]-[% c("var/build_id") %].tar.gz'
    version: '[% c("abbrev") %]'
    merge_aars: |
      #!/bin/bash
      [% c("var/set_default_env") -%]
      [% pc(c('var/compiler'), 'var/setup', {
          compiler_tarfile => c('input_files_by_name/' _ c('var/compiler')),
          gradle_tarfile => c("input_files_by_name/gradle"),
        }) %]
      distdir=/var/tmp/dist
      builddir=/var/tmp/build
      mkdir -p /var/tmp/build
      mkdir -p $distdir/[% project %]

      cat > get-moz-build-date << "EOF"
      [% INCLUDE "get-moz-build-date" %]
      EOF

      tar -C $distdir -xf [% c('input_files_by_name/node') %]
      export PATH="/var/tmp/dist/node/bin:$PATH"

      tar -C $builddir -xf [% c('input_files_by_name/geckoview_armv7') %]
      tar -C $builddir -xf [% c('input_files_by_name/geckoview_aarch64') %]
      tar -C $builddir -xf [% c('input_files_by_name/geckoview_x86') %]
      tar -C $builddir -xf [% c('input_files_by_name/geckoview_x86_64') %]
      tar -C $builddir -xf [% project %]-[% c('version') %].tar.gz

      # Specify the architectures we want to merge
      export MOZ_ANDROID_FAT_AAR_ARCHITECTURES=armeabi-v7a,arm64-v8a,x86,x86_64
      export MOZ_ANDROID_FAT_AAR_ARMEABI_V7A=$builddir/geckoview/*armeabi-v7a*.aar
      export MOZ_ANDROID_FAT_AAR_ARM64_V8A=$builddir/geckoview/*arm64-v8a*.aar
      # Specifying just "x86" is not differentiating enough
      export MOZ_ANDROID_FAT_AAR_X86=$builddir/geckoview/*x86-*.aar
      export MOZ_ANDROID_FAT_AAR_X86_64=$builddir/geckoview/*x86_64*.aar

      cd $builddir/[% project %]-[% c("version") %]
      ln -s mozconfig-android-all .mozconfig
      echo 'mk_add_options MOZ_PARALLEL_BUILD=[% c("num_procs") %]' >> .mozconfig

      eval $(perl $rootdir/get-moz-build-date [% c("var/copyright_year") %] [% c("var/torbrowser_version") %])
      if [ -z $MOZ_BUILD_DATE ]
      then
          echo "MOZ_BUILD_DATE is not set"
          exit 1
      fi

      export JAVA_HOME=/usr/lib/jvm/java-1.11.0-openjdk-amd64
      gradle_repo=/var/tmp/dist/gradle-dependencies
      export GRADLE_MAVEN_REPOSITORIES="file://$gradle_repo","file://$gradle_repo/maven2"
      export GRADLE_FLAGS="--no-daemon --offline"
      mv $rootdir/[% c('input_files_by_name/gradle-dependencies') %] $gradle_repo
      cp -r $gradle_repo/m2/* $gradle_repo

      # We unbreak mach, see: https://bugzilla.mozilla.org/show_bug.cgi?id=1656993.
      export MACH_BUILD_PYTHON_NATIVE_PACKAGE_SOURCE=system
      # Create .mozbuild to avoid interactive prompt in configure
      mkdir "$HOME/.mozbuild"
      # We still need to specify --tor-browser-version due to bug 34005.
      ./mach configure \
        --with-base-browser-version=[% c("var/torbrowser_version") %] \
        [% IF !c("var/rlbox") -%]--without-wasm-sandboxed-libraries[% END %]

      ./mach build --verbose
      find obj-* -regex '.*geckoview.*omni.*\(aar\|pom\)' -exec cp {} $distdir/[% project %] \;

      cd $distdir/
      [% c('tar', {
              tar_src => [ project ],
              tar_args => '-czf ' _ dest_dir _ '/' _ c('filename'),
         }) %]


    input_files:
      - project: container-image
        pkg_type: build
      - name: '[% c("var/compiler") %]'
        project: '[% c("var/compiler") %]'
        pkg_type: build
      - project: gradle
        name: gradle
        pkg_type: build
      - project: node
        name: node
        pkg_type: build
      - filename: 'gradle-dependencies-[% c("var/gradle_dependencies_version") %]'
        name: gradle-dependencies
        exec: '[% INCLUDE "fetch-gradle-dependencies" %]'
      - name: geckoview_armv7
        project: geckoview
        pkg_type: build
        target_prepend:
          - torbrowser-android-armv7
      - name: geckoview_aarch64
        project: geckoview
        pkg_type: build
        target_prepend:
          - torbrowser-android-aarch64
      - name: geckoview_x86
        project: geckoview
        pkg_type: build
        target_prepend:
          - torbrowser-android-x86
      - name: geckoview_x86_64
        project: geckoview
        pkg_type: build
        target_prepend:
          - torbrowser-android-x86_64

  list_toolchain_updates:
    git_url: https://github.com/mozilla/gecko-dev.git
    git_hash: esr102
    tag_gpg_id: 0
    input_files: []
    container:
      use_container: 0

targets:
  nightly:
    git_hash: 'tor-browser-[% c("var/geckoview_version") %]-[% c("var/browser_branch") %]'
    tag_gpg_id: 0

input_files:
  - project: container-image
  - name: '[% c("var/compiler") %]'
    project: '[% c("var/compiler") %]'
  - project: gradle
    name: gradle
  - project: binutils
    name: binutils
  - project: rust
    name: rust
  - project: cbindgen
    name: cbindgen
  - project: node
    name: node
  - project: nasm
    name: nasm
  - project: clang
    name: clang
  - project: 'compiler-rt'
    name: 'compiler-rt'
  - project: wasi-sysroot
    name: wasi-sysroot
    enable: '[% c("var/rlbox") %]'
  - filename: 'gradle-dependencies-[% c("var/gradle_dependencies_version") %]'
    name: gradle-dependencies
    exec: '[% INCLUDE "fetch-gradle-dependencies" %]'
